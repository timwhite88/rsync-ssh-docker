# https://hub.docker.com/_/alpine
ARG alpine_ver=3.19
FROM alpine:${alpine_ver}

ARG build_rev=1


# Install Rsync and SSH.
RUN apk add --no-cache \
            rsync \
            openssh-client-default \
            sshpass \
            ca-certificates \
            tzdata \
            bash \
            curl \
            flock \
 && update-ca-certificates 
